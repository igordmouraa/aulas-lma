const {createApp} = Vue;
createApp ({
    data (){
        return {

            randomIndex: 0,
            randomIndexInternet: 0,

            //vetor de imagens locais
            imagensLocais:[
                './Imagens/lua.jpg',
                './Imagens/sol.jpg',
                './Imagens/SENAI_logo.png'

            ],

            imagensInternet:[
                'https://images3.memedroid.com/images/UPLOADED780/619ac72262baa.jpeg',
                'https://i.ytimg.com/vi/ef2tQzAobys/mqdefault.jpg',
                'https://uploads.spiritfanfiction.com/historias/capitulos/202109/a-historia-de-shrek-com-shelly-careca-22980417-140920211858.jpg ',
                'https://i.pinimg.com/236x/5d/99/2b/5d992b88b9685f2fadccd79e5639b42d.jpg',
                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTzqMo25iJRVjkj0BVEkyk5OY-UnisJq7gtQA&usqp=CAU',
                'https://pbs.twimg.com/profile_images/1609532466120851457/pm3ThnFA_400x400.jpg',

            ]

        };//fim return
    },//fim data

    computed:{
        randomImage()
        {
            return this.imagensLocais[this.randomIndex];

        },//fim RandomImage

        randomImageInternet()
        {
            return this.imagensInternet[this.randomIndexInternet];

        },//fim RandomImageInternet

    },//fim computed

    methods:{
        getRandomImage(){

            this.randomIndex = Math.floor(Math.random()*this.imagensLocais.length);

            this.randomIndexInternet = Math.floor(Math.random()*this.imagensInternet.length);

        }//fim getRandomImage
    }//fim methods

}).mount("#app")