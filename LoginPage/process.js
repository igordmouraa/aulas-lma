const {createApp} = Vue;

createApp ({
    data(){
        return{
            username: "",
            password: "",
            error: null,
            sucesso:null,
            //Arrays para armazenamento dos usuários e senhas.
            usuarios: ["admin","cadu"],
            senhas: ["1234","1811"],

                // Variáveis para armazenamento do usuário e senha que será cadastrado
                newUsername: "",
                newPassword: "",
                confirmPassword: "",
        } //Fechamento return 
    }, //Fechamento data

    methods:{
        login(){
            setTimeout(() => {
                if ((this.username === 'Carlos' && this.password === '1118' ) ||
                (this.username === 'Flora' && this.password === '1811')) {
                    alert("Login realizado com sucesso!");
                    this.error = null;
                } //Fim do if
                else{
                    this.error = "Nome ou senha incorretos!";
                } //Fim do else
            }, 500);
        }, //Fechamento login

        loginArray(){
            this.mostrarEntrada = false;
            setTimeout(() => {
                this.mostrarEntrada = true;

                //uso de uma constante para armazenar o número do index one o usuário "procurado"  está "guardado" no array

                const index = this.usuarios.indexOf(this.username);
                if(index !== -1 && this.senhas[index] === this.password){
                    this.error = null;
                    this.sucesso = "Login efetuado com sucesso!";

                    localStorage.setItem("username", this.username);
                    localStorage.setItem("password", this.password);

                }//Fechamento if
                else{
                    this.sucesso = null;
                    this.error = "Usuário ou senha incorretos"
                }//Fechamento else

                this.username = "";
                this.password = "";

            }, 500);
        }, //Fechamento LoginArray

        adicionarUsuario(){
            if (!this.usuarios.includes(this.newUsername) && this.newUsername !== "") {
                if (this.newPassword && this.newPassword === this.confirmPassword) {
                this.usuarios.push(this.newUsername);
                this.senhas.push(this.newPassword);

                localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                localStorage.setItem("senhas", JSON.stringify(this.senhas));

                this.newUsername = "";
                this.newPassword = "";
                this.confirmPassword = "";

                this.sucesso = "Usuário cadastrado com sucesso!";
                this.error = null;
                } //Fechamento if
                else{
                    this.error = "Por favor, digite uma senha válida!";
                    this.sucesso = null;
                }
            } //Fechamento if
            else{
                this.error = "Por favor, informe um usuário diferente que seja válido";
                this.sucesso =null;

            } //Fechamento else
        }, //Fechamento adicionarUsuario

        paginaCadastro(){
            this.mostrarEntrada = false;
            setTimeout(() =>{
                this.mostrarEntrada = true;
                this.sucesso = "Carregando página do cadastro!";
                this.error = null;
            }, 1000);

            setTimeout(() => {
                window.location.href = "cadastro.html"
            })
        }


    }, //Fechamento methods
}).mount("#app"); //Fechamento createApp