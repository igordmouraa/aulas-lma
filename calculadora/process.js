const {createApp} = Vue;
createApp
({
    data()    {

        return{
            display:"0",
            operandoAtual:null,
            operandoAnterior:null,
            operador:null,
        }//Fechamento do return
    },//Fechamento de Função "data"
    methods:{
        handleButtonClick(botao){
            switch(botao){
                case "+":
                case "-":
                case "/":
                case "X":
                    this.handleOperation(botao);
                    break;

                    case ".":
                        this.handleDecimal();
                        break;

                case"=":
                    this.handleEquals();
                    break;

                default:
                    this.handleNumber(botao);
                    break;

            }
        },//Fechamento handeButtonClick
        handleNumber(numero){
                if(this.display === "0")
                {
                    this.display = numero.toString();
                }
                else
                {
                    this.display += numero.toString();  
                }
        },//Fechamento do handleNumber
        
    handleOperation(operacao){
            if(this.operandoAtual !==null){
                this.handleEquals();
            }//fechamento do if
        this.operador = operacao;

        this.operandoAtual = parseFloat(this.display);
            this.display = "0";
            
    
},//Fechamento handOperation

        handleEquals(){
            const displayAtual = parseFloat(this.display);
            if(this.operandoAtual !==null && this.operador !==null){
                switch(this.operador){
                    case "+":
                        this.display = (this.operandoAtual + displayAtual). toString();
                        break;
                    case "-":
                        this.display = (this.operandoAtual - displayAtual). toString();
                        break;
                    case "X":
                        this.display = (this.operandoAtual * displayAtual). toString();
                        break;
                    case "/":
                        this.display = (this.operandoAtual / displayAtual). toString();
                        break;
                }//fim do switch

                this.operandoAnterior = this.operandoAtual;
                this.operandoAtual =null;
                this.operador = null;

            }//fechamento do if
            else
            {
                    this.operandoAnterior = displayAtual;
            }//fechamento else
        },//fechamento do handleEquals

        handleDecimal(){
                if(!this.display.includes(".")){
                    this.display += ".";
                }//fechamento if
        },//fechamento handleDecimal

        handleClear(){
            this.display = "0";
            this.operandoAtual = null;
            this.operandoAnterior = null;
            this.operador = null;
        },//fechamento handleClear


    },//fim do methods

}).mount("#app"); ////Fechamento do "createApp"