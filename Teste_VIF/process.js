const {createApp} = Vue;
createApp({
    data(){

        return{

            testeSpan: false,
            isLampadaLigada: false,

        }//fim return

    },//Fim data

    methods:{
        handleTest: function()
        {
            this.testeSpan = !this.testeSpan;
        },//fim handletest

        toggleLampada: function()
        {
            this.isLampadaLigada = !this.isLampadaLigada;
        },

    },//fim methods

}).mount ("#app");